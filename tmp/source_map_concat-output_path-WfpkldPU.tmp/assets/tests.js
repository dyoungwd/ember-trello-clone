'use strict';

define('kanban/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/edit-card-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/edit-card-form.js should pass ESLint\n\n');
  });

  QUnit.test('components/new-card-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/new-card-form.js should pass ESLint\n\n');
  });

  QUnit.test('components/new-list-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/new-list-form.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/cards.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/cards.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/cards/detail.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/cards/detail.js should pass ESLint\n\n');
  });

  QUnit.test('models/card.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/card.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cards.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cards.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cards/detail.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cards/detail.js should pass ESLint\n\n');
  });
});
define('kanban/tests/helpers/data-transfer', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var c = _ember.default.Object.extend({
    getData: function getData() {
      return this.get('payload');
    },

    setData: function setData(dataType, payload) {
      this.set("data", { dataType: dataType, payload: payload });
    }
  });

  c.reopenClass({
    makeMockEvent: function makeMockEvent(payload) {
      var transfer = this.create({ payload: payload });
      var res = { dataTransfer: transfer };
      res.originalEvent = res;
      res.originalEvent.preventDefault = function () {
        console.log('prevent default');
      };
      res.originalEvent.stopPropagation = function () {
        console.log('stop propagation');
      };
      return res;
    },

    createDomEvent: function createDomEvent(type) {
      var event = document.createEvent("CustomEvent");
      event.initCustomEvent(type, true, true, null);
      event.dataTransfer = {
        data: {},
        setData: function setData(type, val) {
          this.data[type] = val;
        },
        getData: function getData(type) {
          return this.data[type];
        }
      };
      return event;
    }
  });

  exports.default = c;
});
define('kanban/tests/helpers/destroy-app', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    _ember.default.run(application, 'destroy');
    if (window.server) {
      window.server.shutdown();
    }
  }
});
define('kanban/tests/helpers/ember-drag-drop', ['exports', 'ember', 'jquery', 'ember-test', 'kanban/tests/helpers/data-transfer'], function (exports, _ember, _jquery, _emberTest, _dataTransfer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.drag = drag;


  function drop($dragHandle, dropCssPath, dragEvent) {
    var $dropTarget = (0, _jquery.default)(dropCssPath);

    if ($dropTarget.length === 0) {
      throw 'There are no drop targets by the given selector: \'' + dropCssPath + '\'';
    }

    _ember.default.run(function () {
      triggerEvent($dropTarget, 'dragover', _dataTransfer.default.makeMockEvent());
    });

    _ember.default.run(function () {
      triggerEvent($dropTarget, 'drop', _dataTransfer.default.makeMockEvent(dragEvent.dataTransfer.get('data.payload')));
    });

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'dragend', _dataTransfer.default.makeMockEvent());
    });
  }

  function drag(cssPath) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var dragEvent = _dataTransfer.default.makeMockEvent();
    var $dragHandle = (0, _jquery.default)(cssPath);

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'mouseover');
    });

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'dragstart', dragEvent);
    });

    andThen(function () {
      if (options.beforeDrop) {
        options.beforeDrop.call();
      }
    });

    andThen(function () {
      if (options.drop) {
        drop($dragHandle, options.drop, dragEvent);
      }
    });
  }
});
define('kanban/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'ember', 'kanban/tests/helpers/start-app', 'kanban/tests/helpers/destroy-app'], function (exports, _qunit, _ember, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Promise.resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };

  var Promise = _ember.default.RSVP.Promise;
});
define('kanban/tests/helpers/resolver', ['exports', 'kanban/resolver', 'kanban/config/environment'], function (exports, _resolver, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var resolver = _resolver.default.create();

  resolver.namespace = {
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix
  };

  exports.default = resolver;
});
define('kanban/tests/helpers/start-app', ['exports', 'ember', 'kanban/app', 'kanban/config/environment'], function (exports, _ember, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = _ember.default.merge({}, _environment.default.APP);
    attributes = _ember.default.merge(attributes, attrs); // use defaults, but you can override;

    return _ember.default.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('kanban/tests/integration/components/edit-card-form-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('edit-card-form', 'Integration | Component | edit card form', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "X8llzz3e",
      "block": "{\"statements\":[[1,[26,[\"edit-card-form\"]],false]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "dg5nvhQ4",
      "block": "{\"statements\":[[0,\"\\n\"],[6,[\"edit-card-form\"],null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"locals\":[]},null],[0,\"  \"]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('kanban/tests/integration/components/new-card-form-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('new-card-form', 'Integration | Component | new card form', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "ydi3S5Rf",
      "block": "{\"statements\":[[1,[26,[\"new-card-form\"]],false]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "Da3//DtQ",
      "block": "{\"statements\":[[0,\"\\n\"],[6,[\"new-card-form\"],null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"locals\":[]},null],[0,\"  \"]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('kanban/tests/integration/components/new-list-form-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('new-list-form', 'Integration | Component | new list form', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "qZfyxm4s",
      "block": "{\"statements\":[[1,[26,[\"new-list-form\"]],false]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "KRaCkAgN",
      "block": "{\"statements\":[[0,\"\\n\"],[6,[\"new-list-form\"],null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"locals\":[]},null],[0,\"  \"]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('kanban/tests/test-helper', ['kanban/tests/helpers/resolver', 'ember-qunit', 'ember-cli-qunit'], function (_resolver, _emberQunit, _emberCliQunit) {
  'use strict';

  (0, _emberQunit.setResolver)(_resolver.default);
  (0, _emberCliQunit.start)();
});
define('kanban/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/resolver.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/edit-card-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/edit-card-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/new-card-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/new-card-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/new-list-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/new-list-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cards-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cards-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cards/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cards/detail-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/card-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/card-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cards-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cards-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cards/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cards/detail-test.js should pass ESLint\n\n');
  });
});
define('kanban/tests/unit/controllers/cards-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:cards', 'Unit | Controller | cards', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('kanban/tests/unit/controllers/cards/detail-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:cards/detail', 'Unit | Controller | cards/detail', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('kanban/tests/unit/models/card-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForModel)('card', 'Unit | Model | card', {
    // Specify the other units that are required for this test.
    needs: []
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var model = this.subject();
    // let store = this.store();
    assert.ok(!!model);
  });
});
define('kanban/tests/unit/routes/cards-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:cards', 'Unit | Route | cards', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('kanban/tests/unit/routes/cards/detail-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:cards/detail', 'Unit | Route | cards/detail', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
require('kanban/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
