define('kanban/tests/helpers/ember-drag-drop', ['exports', 'ember', 'jquery', 'ember-test', 'kanban/tests/helpers/data-transfer'], function (exports, _ember, _jquery, _emberTest, _dataTransfer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.drag = drag;


  function drop($dragHandle, dropCssPath, dragEvent) {
    var $dropTarget = (0, _jquery.default)(dropCssPath);

    if ($dropTarget.length === 0) {
      throw 'There are no drop targets by the given selector: \'' + dropCssPath + '\'';
    }

    _ember.default.run(function () {
      triggerEvent($dropTarget, 'dragover', _dataTransfer.default.makeMockEvent());
    });

    _ember.default.run(function () {
      triggerEvent($dropTarget, 'drop', _dataTransfer.default.makeMockEvent(dragEvent.dataTransfer.get('data.payload')));
    });

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'dragend', _dataTransfer.default.makeMockEvent());
    });
  }

  function drag(cssPath) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var dragEvent = _dataTransfer.default.makeMockEvent();
    var $dragHandle = (0, _jquery.default)(cssPath);

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'mouseover');
    });

    _ember.default.run(function () {
      triggerEvent($dragHandle, 'dragstart', dragEvent);
    });

    andThen(function () {
      if (options.beforeDrop) {
        options.beforeDrop.call();
      }
    });

    andThen(function () {
      if (options.drop) {
        drop($dragHandle, options.drop, dragEvent);
      }
    });
  }
});