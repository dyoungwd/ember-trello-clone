define('kanban/controllers/cards/detail', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _ember.default.Controller.extend({
    actions: {
      updateCard: function updateCard(list, title) {
        this.model.setProperties({ list: list, title: title });

        this.model.save();
      },
      deleteCard: function deleteCard() {
        var _this = this;

        this.model.destroyRecord().then(function () {
          _this.transitionToRoute('cards');
        });
      }
    }
  });
});