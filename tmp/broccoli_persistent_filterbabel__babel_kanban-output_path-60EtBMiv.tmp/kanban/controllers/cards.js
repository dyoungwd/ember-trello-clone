define('kanban/controllers/cards', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _ember.default.Controller.extend({
    actions: {
      saveCard: function saveCard(list, title) {
        var card = this.store.createRecord('card', {
          list: list, title: title
        });

        card.save();
      },
      updateCard: function updateCard(list, card) {
        card.set('list', list);
        card.save();
      }
    }
  });
});