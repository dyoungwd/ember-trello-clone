define("kanban/templates/cards/detail", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "GrGJLFs8", "block": "{\"statements\":[[11,\"div\",[]],[15,\"class\",\"modal\"],[13],[0,\"\\n  \"],[11,\"div\",[]],[15,\"class\",\"modal-card\"],[13],[0,\"\\n    \"],[11,\"h3\",[]],[13],[0,\"Edit\"],[14],[0,\"\\n\\n    \"],[1,[33,[\"edit-card-form\"],null,[[\"title\",\"list\",\"onsubmit\"],[[33,[\"readonly\"],[[28,[\"model\",\"title\"]]],null],[33,[\"readonly\"],[[28,[\"model\",\"list\"]]],null],[33,[\"action\"],[[28,[null]],\"updateCard\"],null]]]],false],[0,\"\\n\\n    \"],[11,\"div\",[]],[15,\"class\",\"modal-card-footer\"],[13],[0,\"\\n      \"],[11,\"button\",[]],[15,\"type\",\"button\"],[15,\"class\",\"btn btn-delete\"],[16,\"onclick\",[33,[\"action\"],[[28,[null]],\"deleteCard\"],null],null],[13],[0,\"Delete card\"],[14],[0,\"\\n    \"],[14],[0,\"\\n  \"],[14],[0,\"\\n\"],[14],[0,\"\\n\"]],\"locals\":[],\"named\":[],\"yields\":[],\"hasPartials\":false}", "meta": { "moduleName": "kanban/templates/cards/detail.hbs" } });
});