define("kanban/templates/components/object-bin", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "Vdqn0T25", "block": "{\"statements\":[[6,[\"draggable-object-target\"],null,[[\"action\"],[\"handleObjectDropped\"]],{\"statements\":[[0,\"  \"],[11,\"div\",[]],[15,\"class\",\"object-bin-title\"],[13],[1,[26,[\"name\"]],false],[14],[0,\"\\n  \"],[11,\"br\",[]],[13],[14],[0,\"\\n\"],[6,[\"each\"],[[28,[\"model\"]]],null,{\"statements\":[[6,[\"draggable-object\"],null,[[\"action\",\"content\"],[\"handleObjectDragged\",[28,[\"obj\"]]]],{\"statements\":[[0,\"      \"],[18,\"default\",[[28,[\"obj\"]]]],[0,\"\\n\"]],\"locals\":[]},null]],\"locals\":[\"obj\"]},null]],\"locals\":[]},null]],\"locals\":[],\"named\":[],\"yields\":[\"default\"],\"hasPartials\":false}", "meta": { "moduleName": "kanban/templates/components/object-bin.hbs" } });
});