define('kanban/components/new-card-form', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _ember.default.Component.extend({
    actions: {
      toggleForm: function toggleForm() {
        this.set('showForm', true);
      },
      save: function save(ev) {
        ev.preventDefault();

        this.onsubmit(this.list, this.title);
        this.set('title', '');
        this.set('showForm', false);
      }
    }
  });
});