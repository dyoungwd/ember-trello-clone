define('kanban/components/new-list-form', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _ember.default.Component.extend({
    actions: {
      save: function save(ev) {
        ev.preventDefault();

        this.onsubmit(this.list, this.title);
        this.set('list', '');
        this.set('title', '');
      }
    }
  });
});