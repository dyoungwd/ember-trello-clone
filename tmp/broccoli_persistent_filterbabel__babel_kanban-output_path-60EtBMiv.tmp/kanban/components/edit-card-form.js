define('kanban/components/edit-card-form', ['exports', 'ember'], function (exports, _ember) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _ember.default.Component.extend({
    actions: {
      save: function save(ev) {
        ev.preventDefault();

        this.onsubmit(this.list, this.title);
      }
    }
  });
});