define('kanban/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/resolver.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/edit-card-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/edit-card-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/new-card-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/new-card-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/new-list-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/new-list-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cards-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cards-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/cards/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/cards/detail-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/card-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/card-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cards-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cards-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/cards/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/cards/detail-test.js should pass ESLint\n\n');
  });
});