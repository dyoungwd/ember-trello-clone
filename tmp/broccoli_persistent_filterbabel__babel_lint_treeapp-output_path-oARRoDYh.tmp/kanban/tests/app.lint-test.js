define('kanban/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/edit-card-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/edit-card-form.js should pass ESLint\n\n');
  });

  QUnit.test('components/new-card-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/new-card-form.js should pass ESLint\n\n');
  });

  QUnit.test('components/new-list-form.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/new-list-form.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/cards.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/cards.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/cards/detail.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/cards/detail.js should pass ESLint\n\n');
  });

  QUnit.test('models/card.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/card.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cards.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cards.js should pass ESLint\n\n');
  });

  QUnit.test('routes/cards/detail.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/cards/detail.js should pass ESLint\n\n');
  });
});