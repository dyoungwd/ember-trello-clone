define('ember-get-config/index', ['exports', 'kanban/config/environment'], function (exports, _kanbanConfigEnvironment) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _kanbanConfigEnvironment['default'];
    }
  });
});