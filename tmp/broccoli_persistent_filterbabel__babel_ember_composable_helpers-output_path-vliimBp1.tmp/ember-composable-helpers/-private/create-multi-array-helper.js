define('ember-composable-helpers/-private/create-multi-array-helper', ['exports', 'ember', 'ember-array/utils', 'ember-helper', 'ember-metal/utils', 'ember-metal/observer', 'ember-metal/get', 'ember-metal/set', 'ember-utils'], function (exports, _ember, _utils, _emberHelper, _utils2, _observer, _get, _set, _emberUtils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (multiArrayComputed) {
    return _emberHelper.default.extend({
      compute: function compute(_ref) {
        var _ref2 = _toArray(_ref),
            arrays = _ref2.slice(0);

        (0, _set.default)(this, 'arrays', arrays.map(function (obj) {
          if ((0, _utils.isEmberArray)(obj)) {
            return (0, _utils.A)(obj);
          }

          return obj;
        }));

        return (0, _get.default)(this, 'content');
      },


      valuesDidChange: (0, _observer.default)('arrays.[]', function () {
        this._recomputeArrayKeys();

        var arrays = (0, _get.default)(this, 'arrays');
        var arrayKeys = (0, _get.default)(this, 'arrayKeys');

        if ((0, _emberUtils.isEmpty)(arrays)) {
          defineProperty(this, 'content', []);
          return;
        }

        defineProperty(this, 'content', multiArrayComputed.apply(undefined, _toConsumableArray(arrayKeys)));
      }),

      contentDidChange: (0, _observer.default)('content.[]', function () {
        this.recompute();
      }),

      _recomputeArrayKeys: function _recomputeArrayKeys() {
        var _this = this;

        var arrays = (0, _get.default)(this, 'arrays');

        var oldArrayKeys = (0, _get.default)(this, 'arrayKeys') || [];
        var newArrayKeys = arrays.map(idForArray);

        var keysToRemove = oldArrayKeys.filter(function (key) {
          return newArrayKeys.indexOf(key) === -1;
        });

        keysToRemove.forEach(function (key) {
          return (0, _set.default)(_this, key, null);
        });
        arrays.forEach(function (array) {
          return (0, _set.default)(_this, idForArray(array), array);
        });

        (0, _set.default)(this, 'arrayKeys', newArrayKeys);
      }
    });
  };

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  }

  function _toArray(arr) {
    return Array.isArray(arr) ? arr : Array.from(arr);
  }

  var defineProperty = _ember.default.defineProperty;

  var idForArray = function idForArray(array) {
    return '__array-' + (0, _utils2.guidFor)(array);
  };
});