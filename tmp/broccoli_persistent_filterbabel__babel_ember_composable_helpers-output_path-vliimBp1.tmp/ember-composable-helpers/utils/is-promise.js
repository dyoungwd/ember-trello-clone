define('ember-composable-helpers/utils/is-promise', ['exports', 'ember-utils', 'ember-composable-helpers/utils/is-object'], function (exports, _emberUtils, _isObject) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = isPromise;


  function isPromiseLike() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    return (0, _emberUtils.typeOf)(obj.then) === 'function' && (0, _emberUtils.typeOf)(obj.catch) === 'function';
  }

  function isPromise(obj) {
    return (0, _isObject.default)(obj) && isPromiseLike(obj);
  }
});