define('ember-composable-helpers/helpers/next', ['exports', 'ember', 'ember-array/utils', 'ember-composable-helpers/utils/get-index', 'ember-composable-helpers/-private/create-needle-haystack-helper'], function (exports, _ember, _utils, _getIndex, _createNeedleHaystackHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.next = next;
  var get = _ember.default.get,
      isEmpty = _ember.default.isEmpty;
  function next(currentValue, array) {
    var useDeepEqual = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var currentIndex = (0, _getIndex.default)(array, currentValue, useDeepEqual);
    var lastIndex = get(array, 'length') - 1;

    if (isEmpty(currentIndex)) {
      return;
    }

    return currentIndex === lastIndex ? currentValue : (0, _utils.A)(array).objectAt(currentIndex + 1);
  }

  exports.default = (0, _createNeedleHaystackHelper.default)(next);
});