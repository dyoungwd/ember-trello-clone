define('ember-composable-helpers/helpers/toggle-action', ['exports', 'ember-helper', 'ember-composable-helpers/helpers/toggle', 'ember-composable-helpers/-private/closure-action'], function (exports, _emberHelper, _toggle, _closureAction) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var closureToggle = _toggle.toggle;
  if (_closureAction.default) {
    closureToggle[_closureAction.default] = true;
  }

  exports.default = (0, _emberHelper.helper)(closureToggle);
});