define('ember-composable-helpers/helpers/intersect', ['exports', 'ember-computed', 'ember-composable-helpers/-private/create-multi-array-helper'], function (exports, _emberComputed, _createMultiArrayHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = (0, _createMultiArrayHelper.default)(_emberComputed.intersect);
});