define('ember-composable-helpers/helpers/pipe-action', ['exports', 'ember-helper', 'ember-composable-helpers/helpers/pipe', 'ember-composable-helpers/-private/closure-action'], function (exports, _emberHelper, _pipe, _closureAction) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var closurePipe = _pipe.pipe;
  if (_closureAction.default) {
    closurePipe[_closureAction.default] = true;
  }

  exports.default = (0, _emberHelper.helper)(closurePipe);
});